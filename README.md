# Lycan

A program to track time, calendar, weather, and celestial event cycles for your D&D campaign.

## Usage

```
USAGE:
    lycan <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    config     Create a new, or edit an existing, lycan config file
    date       Commands for displaying or changing the current date and time
    help       Prints this message or the help of the given subcommand(s)
    weather    Commands for displaying the current weather
```

### Config File Commands
```
USAGE:
    lycan config <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help       Prints this message or the help of the given subcommand(s)
    new        Create a new lycan config file
    weather    (Re)Generate weather for the year
```

### Create a New Config File
```
USAGE:
    lycan config new [OPTIONS]

FLAGS:
        --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --cycle <cycle>...          Set the number of days in the moon's lunar cycle
    -a, --current-day <day>         Set the current day
    -d, --days <days>...            Set number of days in the calendar month
    -f, --file-name <file name>     Set the config filename [default: lycan-conf.yaml]
    -F, --first-day <first day>     Set the day of the week upon which the first day of the year falls
    -h, --hour <hour>               Set the current hour [default: 12]
    -i, --minute <minute>           Set the current minute [default: 0]
    -n, --current-month <month>     Set the current month
    -m, --month <month name>...     Define a new calendar month
    -M, --moon <moon name>...       Define a new moon
    -s, --season <season>...        Set the season in which the calendar month falls
    -S, --shift <shift>...          Set the number of days to shift the moon's lunar cycle
    -D, --week-day <week day>...    Define a new week-day
    -y, --current-year <year>       The current year in your campaign setting
```
If you don't provide any options, `lycan` will prompt you to enter the missing information.

### Date and Time Commands
```
USAGE:
    lycan date <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    add     Add some amount of time to the current date and time
    help    Prints this message or the help of the given subcommand(s)
    show    Show the current date and time
```

Values being added to the current date and time must be in the following format: &lt;number&gt;[mhd], where 'm' denotes 'minutes', 'h' denotes 'hours', and 'd' denotes days, respectively. For example, to add 30 minutes to the current time, you would execute the following command: `lycan date add 30m`.

### Weather Commands
```
USAGE:
    lycan weather <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help    Prints this message or the help of the given subcommand(s)
    show    Show the current weather
```
Weather is dynamically generated every time you run `lycan weather show`, rather than being saved in the config file. The weather generation logic follows the *Weather in the World* rules found in issue 68 of *Dungeon Magazine*.

## Abstract

One of the best ways to add immersion into your D&amp;D games is to include real-world elements, such as a calendar, seasons, and weather. While coming up with a calendar and maintaining it isn't very difficult, determining weather certainly is; just ask a meteorologist! `lycan` takes care of the difficult work of both maintaining a calendar and determing weather. All you have to do is run `lycan config new`, follow along with the on-screen prompts, and you're good to go!

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see &lt;https://www.gnu.org/licenses/&gt;.