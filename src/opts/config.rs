use std::iter;
use std::str::FromStr;

use structopt::StructOpt;

use crate::config::{
    gen_weather, Calendar, CelestialEvents, Configuration, Datetime, Month, Moon, Season,
};
use crate::utils::{error, get_cardinal, get_input};

#[derive(Debug, StructOpt)]
pub enum Config {
    #[structopt(name = "new")]
    /// Create a new lycan config file
    New(NewConfig),

    #[structopt(name = "weather")]
    /// (Re)Generate weather for the year
    Weather {
        #[structopt(
            name = "file name",
            short = "f",
            long = "file-name",
            default_value = "lycan-conf.yaml"
        )]
        /// Set the config filename
        file_name: String,

        #[structopt(name = "colder", short = "C", parse(from_occurrences))]
        /// Alter the base temperature to be colder; for every occurrence, lower the base temperature by an additional 1d4
        colder: usize,

        #[structopt(name = "hotter", short = "H", parse(from_occurrences))]
        /// Alter the base temperature to be hotter; for every occurrence, raise the base temperature by an additional 1d4
        hotter: usize,
    },
}

#[derive(Debug, StructOpt)]
pub struct NewConfig {
    #[structopt(
        name = "file name",
        short = "f",
        long = "file-name",
        default_value = "lycan-conf.yaml"
    )]
    /// Set the config filename
    pub file_name: String,

    #[structopt(name = "month name", short = "m", long = "month")]
    /// Define a new calendar month
    pub months: Vec<String>,

    #[structopt(name = "days", short = "d", long = "days")]
    /// Set number of days in the calendar month
    pub days: Vec<usize>,

    #[structopt(name = "season", short = "s", long = "season")]
    /// Set the season in which the calendar month falls
    pub seasons: Vec<String>,

    #[structopt(name = "week day", short = "D", long = "week-day")]
    /// Define a new week-day
    pub week_days: Vec<String>,

    #[structopt(name = "first day", short = "F", long = "first-day")]
    /// Set the day of the week upon which the first day of the year falls
    pub first_day: Option<String>,

    #[structopt(name = "moon name", short = "M", long = "moon")]
    /// Define a new moon
    pub moons: Vec<String>,

    #[structopt(name = "cycle", short = "c", long = "cycle")]
    /// Set the number of days in the moon's lunar cycle
    pub lunar_cycles: Vec<usize>,

    #[structopt(name = "shift", short = "S", long = "shift")]
    /// Set the number of days to shift the moon's lunar cycle
    pub lunar_shift: Vec<usize>,

    #[structopt(name = "year", short = "y", long = "current-year")]
    /// The current year in your campaign setting
    pub year: Option<usize>,

    #[structopt(name = "month", short = "n", long = "current-month")]
    /// Set the current month
    pub month: Option<String>,

    #[structopt(name = "day", short = "a", long = "current-day")]
    /// Set the current day
    pub day: Option<usize>,

    #[structopt(name = "hour", short = "h", long = "hour", default_value = "12")]
    /// Set the current hour
    pub hour: usize,

    #[structopt(name = "minute", short = "i", long = "minute", default_value = "0")]
    /// Set the current minute
    pub minute: usize,
}

impl NewConfig {
    pub fn validate(&self) -> Result<Configuration, MissingConfigFields> {
        let months = !self.months.is_empty()
            && !self.days.is_empty()
            && !self.seasons.is_empty()
            && self.months.len() == self.days.len()
            && self.days.len() == self.seasons.len();

        let week_days = !self.week_days.is_empty();

        let first_day = !self.week_days.is_empty()
            && self.first_day.is_some()
            && self.week_days.contains(&self.first_day.as_ref().unwrap());

        let datetime = self.year.is_some()
            && self.month.is_some()
            && (!self.months.is_empty() && self.months.contains(self.month.as_ref().unwrap()))
            && (self.day.is_some()
                && !self.months.is_empty()
                && !self.days.is_empty()
                && self.day.unwrap()
                    < self.days[self
                        .months
                        .iter()
                        .position(|month| month == self.month.as_ref().unwrap())
                        .unwrap()]);

        let moons = !self.moons.is_empty()
            && !self.lunar_cycles.is_empty()
            && self.moons.len() == self.lunar_cycles.len();

        if months && week_days && first_day && datetime && moons {
            let months = self.parse_months();
            Ok(Configuration::new(
                Calendar::new(
                    months.clone(),
                    self.parse_week_days(),
                    self.parse_first_day(),
                ),
                self.parse_datetime(&months),
                CelestialEvents::new(self.parse_moons()),
                gen_weather(&months, 0, 0),
            ))
        } else {
            Err(MissingConfigFields {
                months: !months,
                week_days: !week_days,
                first_day: !first_day,
                datetime: !datetime,
                moons: !moons,
            })
        }
    }

    pub fn prompt_for_months() -> Vec<Month> {
        let total_months = {
            let s = get_input("How many months are in your campaign's calendar? (default: 12)")
                .trim()
                .to_owned();

            if s.is_empty() {
                12
            } else {
                match s.parse::<usize>() {
                    Ok(total_months) => {
                        if total_months == 0 {
                            error(
                                "You must enter a number of months that is greater-than or equal-to 1",
                            )
                        } else {
                            total_months
                        }
                    }
                    Err(_) => error(
                        "You must enter a number of months that is greater-than or equal-to 1",
                    ),
                }
            }
        };

        iter::successors(Some(1_u8), |n| n.checked_add(1))
            .take(total_months)
            .map(|idx| {
                let name = {
                    let s = get_input(&format!(
                        "Enter the name for the {} month:",
                        get_cardinal(idx)
                    )).trim().to_owned();

                    if s.is_empty() {
                        error("You must enter a name for the month");
                    } else {
                        s
                    }
                };

                let days = {
                    let s = get_input(&format!(
                        "Enter the number of days in the month \"{}\" (default: 30):",
                        name
                    )).trim().to_owned();

                    if s.is_empty() {
                        30
                    } else {
                        match s.parse::<usize>() {
                            Ok(days) => {
                                if days == 0 {
                                    error("You must enter a number of days that is greater-than or equal-to 1");
                                } else {
                                    days
                                }
                            }
                            Err(_) => {
                                error("You must enter a number of days that is greater-than or equal-to 1")
                            }
                        }
                    }
                };

                let season = {
                    let s = get_input(&format!(
                        "In what season does \"{}\" fall?\n{}\nYour selection?",
                        name,
                        ["Winter", "Spring", "Summer", "Autumn"]
                            .iter()
                            .enumerate()
                            .map(|(idx, season)| format!("    ({}) {}", idx + 1, season))
                            .collect::<Vec<String>>()
                            .join("\n")
                    ));

                    match s.as_ref() {
                        "1" => Season::Winter,
                        "2" => Season::Spring,
                        "3" => Season::Summer,
                        "4" => Season::Autumn,
                        _ => error("Invalid season choice; you must choose a selection of 1-4"),
                    }
                };

                Month::new(&name, days, season)
            })
            .collect::<Vec<Month>>()
    }

    pub fn parse_months(&self) -> Vec<Month> {
        self.months
            .iter()
            .enumerate()
            .map(|(idx, month)| {
                Month::new(
                    month.as_ref(),
                    self.days[idx],
                    match Season::from_str(self.seasons[idx].as_ref()) {
                        Ok(season) => season,
                        Err(err) => error(format!("{}", err).as_ref()),
                    },
                )
            })
            .collect::<Vec<Month>>()
    }

    pub fn prompt_for_week_days() -> Vec<String> {
        let total_days = {
            let s = get_input("How many days are in your campaign's week? (default: 7)")
                .trim()
                .to_owned();

            if s.is_empty() {
                7
            } else {
                match s.parse::<usize>() {
                    Ok(total_days) => {
                        if total_days == 0 {
                            error("You must enter a number of days greater-than 0")
                        } else {
                            total_days
                        }
                    }
                    Err(_) => error("You must enter a number of days greater-than 0"),
                }
            }
        };

        iter::successors(Some(1_u8), |n| n.checked_add(1))
            .take(total_days)
            .map(|idx| {
                let s = get_input(&format!(
                    "Enter the name for the {} day:",
                    get_cardinal(idx)
                ));

                if s.trim().is_empty() {
                    error("You must enter a name for the day of the week")
                }

                s.trim().to_owned()
            })
            .collect::<Vec<String>>()
    }

    pub fn parse_week_days(&self) -> Vec<String> {
        self.week_days.clone()
    }

    pub fn prompt_for_first_day(week_days: &[String]) -> usize {
        let s = get_input(&format!(
            "What was the first week day in the year?\n{}\nYour selection?",
            week_days
                .iter()
                .enumerate()
                .map(|(idx, day)| format!(
                    "    ({}) {}{}",
                    idx + 1,
                    day,
                    if idx == 0 { " (default)" } else { "" }
                ))
                .collect::<Vec<String>>()
                .join("\n")
        ));

        if s.trim().is_empty() {
            0
        } else {
            match s.parse::<usize>() {
                Ok(choice) => {
                    if choice >= week_days.len() {
                        error("Invalid day choice; you must choose one of the given options")
                    } else {
                        choice
                    }
                }
                Err(_) => error("Invalid day choice; you must choose one of the given options"),
            }
        }
    }

    pub fn parse_first_day(&self) -> usize {
        match &self.first_day {
            Some(first_day) => {
                if self.week_days.contains(&first_day) {
                    self.week_days.iter().position(|day| day == first_day).unwrap()
                } else {
                    error("The first week day of the year must be found within the list of provided day names")
                }
            },
            None => error("The first week day of the year must be found within the list of provided day names")
        }
    }

    pub fn prompt_for_datetime(months: &[Month]) -> Datetime {
        let year = match get_input("What is the current year in your campaign?")
            .trim()
            .parse::<usize>()
        {
            Ok(year) => {
                if year == 0 {
                    error("You must enter a year that is greater-than or equal-to 0")
                } else {
                    year - 1
                }
            }
            Err(_) => error("You must enter a year that is greater-than or equal-to 0"),
        };

        let month = match get_input(&format!(
            "What is the current month in your campaign?\n{}\nYour selection?",
            months
                .iter()
                .enumerate()
                .map(|(idx, month)| format!(
                    "    ({}) {}{}",
                    idx + 1,
                    month.name,
                    if idx == 0 { " (default)" } else { "" }
                ))
                .collect::<Vec<String>>()
                .join("\n")
        ))
        .trim()
        .parse::<usize>()
        {
            Ok(idx) => {
                if idx > months.len() || idx == 0 {
                    error("Invalid month choice; you must choose one of the given options")
                } else {
                    idx - 1
                }
            }
            Err(_) => error("Invalid month choice; you must choose one of the given options"),
        };

        let day = match get_input(&format!(
            "What is the current day of the month of \"{}\"? (default 1)",
            months[month].name
        ))
        .trim()
        .parse::<usize>()
        {
            Ok(day) => {
                if day == 0 || day > months[month].days {
                    error(&format!(
                        "You must enter a day that is greater-than 0, and less-than or equal-to {}",
                        months[month].days
                    ));
                } else {
                    day - 1
                }
            }
            Err(_) => error(&format!(
                "You must enter a day that is greater-than 0, and less-than or equal-to {}",
                months[month].days
            )),
        };

        let hour = match get_input(&format!(
            "What is the current hour in the day, in 24-hour time? (default: 12)"
        ))
        .trim()
        .parse::<usize>()
        {
            Ok(hour) => {
                if hour == 0 || hour > 24 {
                    error("You must enter in a number of hours that is between 1 and 24");
                } else {
                    hour - 1
                }
            }
            Err(_) => 11,
        };

        let minute = match get_input(&format!(
            "What is the current minute of the hour? (default: 0)"
        ))
        .trim()
        .parse::<usize>()
        {
            Ok(minute) => {
                if minute >= 60 {
                    error("You must enter in a number of minutes less-than or equal-to 59");
                } else {
                    minute
                }
            }
            Err(_) => 0,
        };

        Datetime::new(year, month, day, hour, minute, !(months[month].days == 1))
    }

    pub fn parse_datetime(&self, months: &[Month]) -> Datetime {
        let month = months
            .iter()
            .position(|month| &month.name == self.month.as_ref().unwrap())
            .unwrap();

        Datetime::new(
            self.year.unwrap() - 1,
            month,
            self.day.unwrap() - 1,
            if self.hour >= 24 {
                self.hour % 24
            } else {
                self.hour
            },
            self.minute % 60,
            !(months[month].days == 1),
        )
    }

    pub fn prompt_for_moons() -> Vec<Moon> {
        let total_moons = {
            let s = get_input("How many moons exist within your campaign? (default 1)")
                .trim()
                .to_owned();

            if s.is_empty() {
                1
            } else {
                match s.parse::<usize>() {
                    Ok(total_moons) => total_moons,
                    Err(_) => error(
                        "The total number of moons must be a number greater-than or equal-to 0",
                    ),
                }
            }
        };

        iter::successors(Some(1_u8), |n| n.checked_add(1))
            .take(total_moons)
            .map(|idx| {
                let name = get_input(&format!(
                    "Enter the name for the {} moon:",
                    get_cardinal(idx)
                )).trim().to_owned();

                if name.is_empty() {
                    error("You must enter a name for the moon");
                }

                let cycle =
                    match get_input(&format!("How many days are in \"{}'s\" lunar cycle?", name))
                        .trim()
                        .parse::<usize>()
                    {
                        Ok(cycle) => {
                            if cycle == 0 {
                                error("Lunar cycle must a number greater-than or equal-to 1");
                            }

                            cycle
                        }
                        Err(_) => error("Lunar cycle must a number greater-than or equal-to 1"),
                    };

                let shift = {
                    let s = get_input(&format!(
                        "By how many days should the start of \"{}'s\" lunar cycle be shifted? (default 0)",
                        name
                    )).trim().to_owned();

                    if s.is_empty() {
                        0
                    } else {
                        match s.parse::<usize>() {
                            Ok(shift) => shift,
                            Err(_) => {
                                error("Lunar shift value must be a number greater-than or equal-to 0")
                            }
                        }
                    }
                };

                Moon::new(&name, cycle, shift)
            })
            .collect::<Vec<Moon>>()
    }

    pub fn parse_moons(&self) -> Vec<Moon> {
        self.moons
            .iter()
            .enumerate()
            .map(|(idx, moon)| {
                let shift = if self.lunar_shift.is_empty() || self.lunar_shift.len() - 1 < idx {
                    0
                } else {
                    self.lunar_shift[idx]
                };

                Moon::new(moon, self.lunar_cycles[idx], shift)
            })
            .collect::<Vec<Moon>>()
    }
}

pub struct MissingConfigFields {
    pub months: bool,
    pub week_days: bool,
    pub first_day: bool,
    pub datetime: bool,
    pub moons: bool,
}
