use std::path::PathBuf;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub enum Weather {
    #[structopt(name = "show")]
    /// Show the current weather
    Show(ShowWeather),
}

#[derive(Debug, StructOpt)]
pub struct ShowWeather {
    #[structopt(
        name = "file name",
        short = "f",
        long = "filename",
        default_value = "lycan-conf.yaml",
        parse(from_os_str)
    )]
    /// Set the config filename
    pub file_name: PathBuf,

    #[structopt(name = "hills", short = "H", long = "hills")]
    /// Set the current terrain to be hills or rough terrain (default)
    pub hills: bool,

    #[structopt(name = "forest", short = "F", long = "forest")]
    /// Set the current terrain to be forest
    pub forest: bool,

    #[structopt(name = "jungle", short = "J", long = "jungle")]
    /// Set the current terrain to be jungle
    pub jungle: bool,

    #[structopt(name = "swamp", short = "S", long = "swamp")]
    /// Set the current terrain to be swamp or marsh
    pub swamp: bool,

    #[structopt(name = "plains", short = "P", long = "plains")]
    /// Set the current terrain to be plains
    pub plains: bool,

    #[structopt(name = "desert", short = "D", long = "desert")]
    /// Set the current terrain to be desert
    pub desert: bool,

    #[structopt(name = "mountains", short = "M", long = "mountains")]
    /// Set the current terrain to be mountains
    pub mountains: bool,

    #[structopt(
        name = "elevation",
        short = "m",
        long = "elevation",
        default_value = "0"
    )]
    /// Set the current elevation while in the mountains
    pub elevation: usize,

    #[structopt(name = "coast", short = "C", long = "coast")]
    /// Set the current terrain to be sea coast
    pub coast: bool,

    #[structopt(name = "sea", short = "E", long = "sea")]
    /// Set the current terrain to be at sea
    pub sea: bool,
}

impl ShowWeather {
    pub fn get_terrain(&self) -> Terrain {
        if self.forest {
            Terrain::Forest
        } else if self.jungle {
            Terrain::Jungle
        } else if self.swamp {
            Terrain::Swamp
        } else if self.plains {
            Terrain::Plains
        } else if self.desert {
            Terrain::Desert
        } else if self.mountains {
            Terrain::Mountains(self.elevation)
        } else if self.coast {
            Terrain::Coast
        } else if self.sea {
            Terrain::Sea
        } else {
            Terrain::Hills
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Terrain {
    Hills,
    Forest,
    Jungle,
    Swamp,
    Plains,
    Desert,
    Mountains(usize),
    Coast,
    Sea,
}
