use std::path::PathBuf;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub enum Date {
    #[structopt(name = "show")]
    /// Show the current date and time
    Show {
        #[structopt(
            name = "file name",
            short = "f",
            long = "file-name",
            default_value = "lycan-conf.yaml",
            parse(from_os_str)
        )]
        /// Set the config filename
        file_name: PathBuf,
    },

    #[structopt(name = "add")]
    /// Add some amount of time to the current date and time
    Add {
        #[structopt(
            name = "file name",
            short = "f",
            long = "file-name",
            default_value = "lycan-conf.yaml",
            parse(from_os_str)
        )]
        file_name: PathBuf,

        /// The value to add to the current date and time; must be in the form of <number>[mhd]
        value: String,
    },
}
