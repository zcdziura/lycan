mod config;
mod opts;
mod utils;

use std::fs;

use serde_yaml;

use config::{gen_weather, Configuration};
use opts::{Config, Date, Opt, Weather};

#[paw::main]
fn main(opt: opts::Opt) {
    match opt {
        Opt::Config(command) => match command {
            Config::New(new_config) => {
                let config = Configuration::from(&new_config);
                fs::write(
                    &new_config.file_name,
                    serde_yaml::to_string(&config).unwrap(),
                )
                .unwrap();
            }
            Config::Weather {
                file_name,
                colder,
                hotter,
            } => {
                let mut config: Configuration = serde_yaml::from_slice(
                    fs::read(&file_name)
                        .expect("Unable to open config file")
                        .as_slice(),
                )
                .unwrap();

                config.weather = gen_weather(&config.calendar.months, colder, hotter);
                fs::write(&file_name, serde_yaml::to_string(&config).unwrap()).unwrap();
            }
        },
        Opt::Date(command) => match command {
            Date::Show { file_name } => {
                let config: Configuration = serde_yaml::from_slice(
                    fs::read(&file_name)
                        .expect("Unable to open config file")
                        .as_slice(),
                )
                .unwrap();

                println!("{}", config.get_datetime());
            }
            Date::Add { file_name, value } => {
                let mut config: Configuration = serde_yaml::from_slice(
                    fs::read(&file_name)
                        .expect("Unable to open config file")
                        .as_slice(),
                )
                .unwrap();

                config.add_time(&value);
                fs::write(&file_name, serde_yaml::to_string(&config).unwrap()).unwrap();
                println!("{}", config.get_datetime());
            }
        },
        Opt::Weather(command) => match command {
            Weather::Show(show) => {
                let config: Configuration = serde_yaml::from_slice(
                    fs::read(&show.file_name)
                        .expect("Unable to open config file")
                        .as_slice(),
                )
                .unwrap();

                println!("{}", config.get_current_weather(show.get_terrain()));
            }
        },
    };
}
