mod config;
mod date;
mod weather;

use structopt::StructOpt;

pub use config::*;
pub use date::*;
pub use weather::*;

#[derive(Debug, StructOpt)]
pub enum Opt {
    #[structopt(name = "config")]
    /// Create a new, or edit an existing, lycan config file
    Config(config::Config),

    #[structopt(name = "date")]
    /// Commands for displaying or changing the current date and time
    Date(Date),

    #[structopt(name = "weather")]
    /// Commands for displaying the current weather
    Weather(Weather),
}
