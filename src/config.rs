mod calendar;
mod celestial_events;
mod datetime;
mod weather;

use std::convert::From;

use rand::prelude::*;
use serde::{Deserialize, Serialize};

use crate::opts::{MissingConfigFields, NewConfig, Terrain};
pub use calendar::*;
pub use celestial_events::*;
pub use datetime::*;
pub use weather::*;

#[derive(Clone, Deserialize, Serialize)]
pub struct Configuration {
    pub calendar: Calendar,
    pub datetime: Datetime,
    pub celestial_events: CelestialEvents,
    pub weather: Vec<Weather>,
}

impl Configuration {
    pub fn new(
        calendar: Calendar,
        datetime: Datetime,
        celestial_events: CelestialEvents,
        weather: Vec<Weather>,
    ) -> Self {
        Configuration {
            calendar,
            datetime,
            celestial_events,
            weather,
        }
    }

    fn get_day_of_week(&self, day_of_year: usize) -> usize {
        (day_of_year + self.calendar.first_day) % self.calendar.week_days.len()
    }

    pub fn get_current_weather(&self, terrain: Terrain) -> String {
        let weather = &self.weather[self.datetime.get_day_of_year(&self.calendar.months)];
        let mut rngesus = thread_rng();
        let (temp_low_modifier, temp_high_modifier, precip_chance_modifier, wind_speed_modifier) =
            match terrain {
                Terrain::Hills => (
                    0_isize,
                    0_isize,
                    0_isize,
                    if rngesus.gen::<bool>() {
                        5_isize
                    } else {
                        -5_isize
                    },
                ),
                Terrain::Forest => (0, 0, -5, -5),
                Terrain::Jungle => (5, 5, 10, -10),
                Terrain::Swamp => (5, 5, 5, -5),
                Terrain::Plains => (0, 0, 0, 5),
                Terrain::Desert => (-10, 10, -30, 5),
                Terrain::Mountains(elevation) => (
                    (elevation as isize / 1000) * -3,
                    (elevation as isize / 1000) * -3,
                    0,
                    (elevation as isize / 1000) * 5,
                ),
                Terrain::Coast => (
                    if weather.temp_high < 30 { -5 } else { 0 },
                    if weather.temp_low > 70 { 5 } else { 0 },
                    5,
                    5,
                ),
                Terrain::Sea => (
                    if weather.temp_high < 30 { -10 } else { 0 },
                    if weather.temp_low > 70 { 5 } else { 0 },
                    15,
                    10,
                ),
            };

        let temp_low = weather.temp_low + temp_low_modifier;
        let temp_high = weather.temp_high + temp_high_modifier;
        let precipitation_type = if (rngesus.gen_range(0, 100) as isize)
            < weather.precip_chance as isize + precip_chance_modifier
        {
            PrecipitationType::from_temp_and_terrain(
                rngesus.gen_range(0, 100),
                weather.temp_low + temp_low_modifier,
                weather.temp_high + temp_high_modifier,
                terrain,
            )
        } else {
            PrecipitationType::None(1, 20)
        };

        let mut rngesus = thread_rng();
        match &precipitation_type {
            PrecipitationType::HeavyBlizzard(effect)
            | PrecipitationType::Blizzard(effect)
            | PrecipitationType::HeavySnowstorm(effect)
            | PrecipitationType::LightSnowstorm(effect)
            | PrecipitationType::Sleetstorm(effect)
            | PrecipitationType::Hailstorm(effect)
            | PrecipitationType::HeavyFog(effect)
            | PrecipitationType::LightFog(effect)
            | PrecipitationType::Mist(effect)
            | PrecipitationType::Drizzle(effect)
            | PrecipitationType::LightRainstorm(effect)
            | PrecipitationType::HeavyRainstorm(effect)
            | PrecipitationType::Thunderstorm(effect)
            | PrecipitationType::TropicalStorm(effect)
            | PrecipitationType::Monsoon(effect)
            | PrecipitationType::Gale(effect)
            | PrecipitationType::Hurricane(effect) => format!(
                "There will be {}\nTemperatures will reach a high of {}°, with a low of {}°.\nVision will {}, and anyone traveling overland will {}.",
                {
                    let mut s = precipitation_type.to_string() + match &precipitation_type {
                        PrecipitationType::HeavyBlizzard(_)
                        | PrecipitationType::Blizzard(_)
                        | PrecipitationType::HeavySnowstorm(_)
                        | PrecipitationType::LightSnowstorm(_)
                        | PrecipitationType::Sleetstorm(_)
                        | PrecipitationType::Hailstorm(_)
                        | PrecipitationType::HeavyFog(_)
                        | PrecipitationType::LightFog(_)
                        | PrecipitationType::Mist(_)
                        | PrecipitationType::Drizzle(_)
                        | PrecipitationType::LightRainstorm(_)
                        | PrecipitationType::HeavyRainstorm(_)
                        | PrecipitationType::Thunderstorm(_)
                        | PrecipitationType::TropicalStorm(_)
                        | PrecipitationType::Monsoon(_)
                            => " that lasts ",
                        PrecipitationType::Gale(_)
                        | PrecipitationType::Hurricane(_)
                            => " lasting for ",
                        _ => unreachable!("")
                    } + &format!("{} {}, ", rngesus.gen_range(effect.duration_min, effect.duration_max + 1), effect.duration_type)
                    + &format!("with wind gusts up to {} miles-per-hour", rngesus.gen_range(effect.wind_speed_min, effect.wind_speed_max + 1) as isize + wind_speed_modifier);

                    if rngesus.gen_range(0, 100) < effect.continuing_chance {
                        match rngesus.gen_range(0, 10) {
                            val @ 0 | val @ 9 => {
                                let subseq_precip = if val == 0 { precipitation_type.get_prev_precip_roll() } else { precipitation_type.get_next_precip_roll() };
                                s.push_str(&format!(
                                    ", after which it will turn into {} that will last for an additional {} {}, with wind gusts up to {} miles-per-hour",
                                    subseq_precip.to_string(),
                                    rngesus.gen_range(effect.duration_min, effect.duration_max + 1),
                                    effect.duration_type,
                                    rngesus.gen_range(
                                        subseq_precip.get_effect().unwrap().wind_speed_min,
                                        subseq_precip.get_effect().unwrap().wind_speed_max + 1) as isize + wind_speed_modifier));
                            },
                            _ => {}
                        }
                    }

                    s + "."
                },
                temp_high,
                temp_low,
                match effect.vision_obscurity {
                    VisionObscurity::None => "not be obscured",
                    VisionObscurity::Light => "be lightly obscured",
                    VisionObscurity::Heavy => "be heavily obscured"
                },
                if effect.navigation_penalty == 0 {
                    "not suffer any navigation penalties".to_owned()
                } else {
                    format!("suffer a {} navigation penalty", effect.navigation_penalty)
                }
            ),
            PrecipitationType::None(wind_speed_min, wind_speed_max) => {
                let wind_speed = rngesus.gen_range(wind_speed_min, wind_speed_max + 1) as isize
                    + wind_speed_modifier;
                format!(
                    "The sky will be {}, with a high of {}° and a low of {}°.\nThere will be {}.",
                    match weather.sky_condition {
                        SkyCondition::Clear => "clear",
                        SkyCondition::PartlyCloudy => "partly cloudy",
                        SkyCondition::Cloudy => "cloudy",
                    },
                    temp_high,
                    temp_low,
                    if wind_speed <= 0 {
                        "no noticeable wind".to_owned()
                    } else {
                        format!(
                            "a {} of about {} miles-per-hour",
                            if wind_speed <= 10 {
                                "steady breeze"
                            } else if wind_speed <= 25 {
                                "steady wind"
                            } else {
                                "heavy wind"
                            },
                            wind_speed
                        )
                    }
                )
            }
        }
    }

    pub fn get_datetime(&self) -> String {
        let hour = if self.datetime.hour == 0 {
            12
        } else if self.datetime.hour > 12 {
            self.datetime.hour % 12
        } else {
            self.datetime.hour
        };

        let section_of_day = match self.datetime.hour {
            0..=3 => SectionOfDay::LateNight,
            4..=7 => SectionOfDay::EarlyMorning,
            8..=11 => SectionOfDay::Morning,
            12..=15 => SectionOfDay::Afternoon,
            16..=19 => SectionOfDay::Evening,
            20..=24 => SectionOfDay::Night,
            _ => unreachable!(),
        };

        let day_of_year = self.datetime.get_day_of_year(&self.calendar.months);
        format!(
            "It is {:02}:{:02}, {}, on {}, {}{}, in the year {}\n{}",
            hour,
            self.datetime.minute,
            section_of_day,
            self.calendar.week_days[self.get_day_of_week(day_of_year)],
            if self.datetime.should_print_day {
                (self.datetime.day + 1).to_string() + " "
            } else {
                "".to_owned()
            },
            self.calendar.months[self.datetime.month].name,
            self.datetime.year + 1,
            self.celestial_events
                .moons
                .iter()
                .map(|moon| format!(
                    "The moon \"{}\" is {}",
                    moon.name,
                    moon.get_phase(day_of_year)
                ))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }

    pub fn add_time(&mut self, time: &str) {
        let old_year = self.datetime.year;
        self.datetime.add_time(time, &self.calendar.months);

        if self.datetime.year > old_year {
            let next_year = self
                .calendar
                .months
                .iter()
                .map(|month| month.days)
                .fold(0, |acc, curr| acc + curr);
            self.calendar.first_day = self.get_day_of_week(next_year);
            self.celestial_events.moons.iter_mut().for_each(|moon| {
                moon.shift = usize::from(moon.get_phase(next_year)) * (moon.cycle / 8);
            });
            self.weather = gen_weather(&self.calendar.months, 0, 0);
        }
    }
}

impl From<&NewConfig> for Configuration {
    fn from(new_config: &NewConfig) -> Self {
        match new_config.validate() {
            Ok(config) => config,
            Err(missing_config_fields) => {
                let MissingConfigFields {
                    months: missing_months,
                    week_days: missing_week_days,
                    first_day: missing_first_day,
                    datetime: missing_datetime,
                    moons: missing_moons,
                } = missing_config_fields;

                let months = if missing_months {
                    NewConfig::prompt_for_months()
                } else {
                    new_config.parse_months()
                };

                let week_days = if missing_week_days {
                    NewConfig::prompt_for_week_days()
                } else {
                    new_config.parse_week_days()
                };

                let first_day = if missing_first_day {
                    NewConfig::prompt_for_first_day(week_days.as_ref())
                } else {
                    new_config.parse_first_day()
                };

                let datetime = if missing_datetime {
                    NewConfig::prompt_for_datetime(&months)
                } else {
                    new_config.parse_datetime(&months)
                };

                let moons = if missing_moons {
                    NewConfig::prompt_for_moons()
                } else {
                    new_config.parse_moons()
                };

                Configuration::new(
                    Calendar::new(months.clone(), week_days, first_day),
                    datetime,
                    CelestialEvents::new(moons),
                    gen_weather(&months, 0, 0),
                )
            }
        }
    }
}
