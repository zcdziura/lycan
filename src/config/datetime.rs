use std::fmt;

use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::config::Month;
use crate::utils::error;

#[derive(Clone, Deserialize, Serialize)]
pub struct Datetime {
    pub year: usize,
    pub month: usize,
    pub day: usize,
    pub hour: usize,
    pub minute: usize,
    pub should_print_day: bool,
}

impl Datetime {
    pub fn new(
        year: usize,
        month: usize,
        day: usize,
        hour: usize,
        minute: usize,
        should_print_day: bool,
    ) -> Self {
        Datetime {
            year,
            month,
            day,
            hour,
            minute,
            should_print_day,
        }
    }

    fn from_mins(mins: usize, months: &[Month]) -> Self {
        let mut minutes = mins;
        let year = {
            let year = minutes / 525_600;
            minutes = minutes - (year * 525_600);
            year
        };

        let mut day = {
            let day = minutes / 1_440;
            minutes = minutes - (day * 1440);
            day
        };

        let month = months.len()
            - months
                .iter()
                .skip_while(|month| {
                    if day >= month.days {
                        day = day - month.days;
                        true
                    } else {
                        false
                    }
                })
                .count();

        let hour = {
            let hour = minutes / 60;
            minutes = minutes - (hour * 60);
            hour
        };

        Datetime::new(year, month, day, hour, minutes, months[month].days > 1)
    }

    fn into_mins(&self, months: &[Month]) -> usize {
        self.year * 525_600
            + ((months[0..self.month]
                .iter()
                .map(|month| month.days)
                .fold(0, |acc, day| acc + day)
                + self.day)
                * 1_440)
            + (self.hour * 60)
            + self.minute
    }

    pub fn add_time(&mut self, time: &str, months: &[Month]) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(?P<value>\d+)(?P<suffix>[dhm])").unwrap();
        }

        match RE.captures(time) {
            Some(caps) => {
                let addend = caps
                    .iter()
                    .enumerate()
                    .skip(1)
                    .map(|(idx, cap)| {
                        if idx == 1 {
                            cap.map(|val| val.as_str().parse::<usize>().unwrap())
                                .unwrap()
                        } else {
                            cap.map(|val| match val.as_str() {
                                "d" => 1_440,
                                "h" => 60,
                                "m" => 1,
                                _ => unreachable!(),
                            })
                            .unwrap()
                        }
                    })
                    .fold(1, |acc, v| acc * v);

                let new_datetime = self.into_mins(months) + addend;

                *self = Datetime::from_mins(new_datetime, months);
            }
            None => error(&format!(
                "Invalid time value {}; must be in the form of <number>[dhm]",
                time
            )),
        };
    }

    pub fn get_day_of_year(&self, months: &[Month]) -> usize {
        months
            .iter()
            .take_while(|month| month.name != months[self.month].name)
            .map(|month| month.days)
            .fold(0, |acc, curr| acc + curr)
            + self.day
    }
}

#[derive(Debug)]
pub enum SectionOfDay {
    LateNight,
    EarlyMorning,
    Morning,
    Afternoon,
    Evening,
    Night,
}

impl fmt::Display for SectionOfDay {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                SectionOfDay::LateNight => "late in the night",
                SectionOfDay::EarlyMorning => "early in the morning",
                SectionOfDay::Morning => "morning",
                SectionOfDay::Afternoon => "afternoon",
                SectionOfDay::Evening => "evening",
                SectionOfDay::Night => "nighttime",
            }
        )
    }
}
