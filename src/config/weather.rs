use std::convert::From;
use std::iter;
use std::string::ToString;

use rand::prelude::*;
use serde::{Deserialize, Serialize};

use crate::config::{Month, Season};
use crate::opts::Terrain;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Weather {
    pub temp_low: isize,
    pub temp_high: isize,
    pub sky_condition: SkyCondition,
    pub precip_chance: usize,
}

impl Weather {
    pub fn new(
        season: Season,
        part_of_season: PartOfSeason,
        base_temp_modifier: BaseTemperatureModifier,
        colder: usize,
        hotter: usize,
    ) -> Weather {
        let mut rngesus = thread_rng();
        let mut temp_base: isize = iter::repeat(rngesus.gen_range(1, 5) as isize)
            .take(hotter)
            .sum::<isize>()
            * 10
            - iter::repeat(rngesus.gen_range(1, 5) as isize)
                .take(colder)
                .sum::<isize>()
                * 10;

        let rand_sky_condition: usize = rngesus.gen_range(0, 100);
        let (temp_low, temp_high, sky_condition, precip_chance) = match season {
            Season::Winter => match part_of_season {
                PartOfSeason::FirstThird => {
                    temp_base += 33
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -63,
                            BaseTemperatureModifier::SevereRecordLow => -42,
                            BaseTemperatureModifier::RecordLow => -21,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 14,
                            BaseTemperatureModifier::SevereRecordHigh => 21,
                            BaseTemperatureModifier::ExtremeRecordHigh => 42,
                        };

                    (
                        temp_base - rngesus.gen_range(1, 21),
                        temp_base + rngesus.gen_range(6, 14),
                        match rand_sky_condition {
                            0..=24 => SkyCondition::Clear,
                            25..=49 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        43,
                    )
                }
                PartOfSeason::SecondThird => {
                    temp_base += 32
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -63,
                            BaseTemperatureModifier::SevereRecordLow => -42,
                            BaseTemperatureModifier::RecordLow => -21,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 14,
                            BaseTemperatureModifier::SevereRecordHigh => 21,
                            BaseTemperatureModifier::ExtremeRecordHigh => 42,
                        };

                    (
                        temp_base - rngesus.gen_range(1, 21),
                        temp_base + rngesus.gen_range(6, 14),
                        match rand_sky_condition {
                            0..=22 => SkyCondition::Clear,
                            23..=49 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        46,
                    )
                }
                PartOfSeason::LastThird => {
                    temp_base += 34
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -63,
                            BaseTemperatureModifier::SevereRecordLow => -42,
                            BaseTemperatureModifier::RecordLow => -21,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 11,
                            BaseTemperatureModifier::SevereRecordHigh => 22,
                            BaseTemperatureModifier::ExtremeRecordHigh => 33,
                        };

                    (
                        temp_base - rngesus.gen_range(5, 15),
                        temp_base + rngesus.gen_range(5, 11),
                        match rand_sky_condition {
                            0..=24 => SkyCondition::Clear,
                            25..=49 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        40,
                    )
                }
            },
            Season::Spring => match part_of_season {
                PartOfSeason::FirstThird => {
                    temp_base += 42
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -45,
                            BaseTemperatureModifier::SevereRecordLow => -30,
                            BaseTemperatureModifier::RecordLow => -15,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 11,
                            BaseTemperatureModifier::SevereRecordHigh => 22,
                            BaseTemperatureModifier::ExtremeRecordHigh => 33,
                        };

                    (
                        temp_base - rngesus.gen_range(5, 15),
                        temp_base + rngesus.gen_range(5, 11),
                        match rand_sky_condition {
                            0..=26 => SkyCondition::Clear,
                            27..=53 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        44,
                    )
                }
                PartOfSeason::SecondThird => {
                    temp_base += 52
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -63,
                            BaseTemperatureModifier::SevereRecordLow => -42,
                            BaseTemperatureModifier::RecordLow => -21,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 14,
                            BaseTemperatureModifier::SevereRecordHigh => 21,
                            BaseTemperatureModifier::ExtremeRecordHigh => 42,
                        };

                    (
                        temp_base - rngesus.gen_range(5, 15),
                        temp_base + rngesus.gen_range(5, 13),
                        match rand_sky_condition {
                            0..=19 => SkyCondition::Clear,
                            20..=54 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        42,
                    )
                }
                PartOfSeason::LastThird => {
                    temp_base += 63
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -51,
                            BaseTemperatureModifier::SevereRecordLow => -34,
                            BaseTemperatureModifier::RecordLow => -17,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 17,
                            BaseTemperatureModifier::SevereRecordHigh => 34,
                            BaseTemperatureModifier::ExtremeRecordHigh => 51,
                        };

                    (
                        temp_base - rngesus.gen_range(7, 17),
                        temp_base + rngesus.gen_range(7, 17),
                        match rand_sky_condition {
                            0..=19 => SkyCondition::Clear,
                            20..=52 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        42,
                    )
                }
            },
            Season::Summer => match part_of_season {
                PartOfSeason::FirstThird => {
                    temp_base += 71
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -39,
                            BaseTemperatureModifier::SevereRecordLow => -26,
                            BaseTemperatureModifier::RecordLow => -13,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 17,
                            BaseTemperatureModifier::SevereRecordHigh => 34,
                            BaseTemperatureModifier::ExtremeRecordHigh => 51,
                        };

                    (
                        temp_base - rngesus.gen_range(7, 13),
                        temp_base + rngesus.gen_range(9, 17),
                        match rand_sky_condition {
                            0..=19 => SkyCondition::Clear,
                            20..=59 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        36,
                    )
                }
                PartOfSeason::SecondThird => {
                    temp_base += 77
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -39,
                            BaseTemperatureModifier::SevereRecordLow => -26,
                            BaseTemperatureModifier::RecordLow => -13,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 11,
                            BaseTemperatureModifier::SevereRecordHigh => 22,
                            BaseTemperatureModifier::ExtremeRecordHigh => 33,
                        };

                    (
                        temp_base - rngesus.gen_range(7, 13),
                        temp_base + rngesus.gen_range(5, 11),
                        match rand_sky_condition {
                            0..=21 => SkyCondition::Clear,
                            22..=61 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        33,
                    )
                }
                PartOfSeason::LastThird => {
                    temp_base += 75
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -39,
                            BaseTemperatureModifier::SevereRecordLow => -26,
                            BaseTemperatureModifier::RecordLow => -13,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 11,
                            BaseTemperatureModifier::SevereRecordHigh => 22,
                            BaseTemperatureModifier::ExtremeRecordHigh => 33,
                        };

                    (
                        temp_base - rngesus.gen_range(7, 13),
                        temp_base + rngesus.gen_range(7, 11),
                        match rand_sky_condition {
                            0..=24 => SkyCondition::Clear,
                            25..=59 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        33,
                    )
                }
            },
            Season::Autumn => match part_of_season {
                PartOfSeason::FirstThird => {
                    temp_base += 68
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -45,
                            BaseTemperatureModifier::SevereRecordLow => -30,
                            BaseTemperatureModifier::RecordLow => -15,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 15,
                            BaseTemperatureModifier::SevereRecordHigh => 30,
                            BaseTemperatureModifier::ExtremeRecordHigh => 45,
                        };

                    (
                        temp_base - rngesus.gen_range(7, 15),
                        temp_base + rngesus.gen_range(7, 15),
                        match rand_sky_condition {
                            0..=32 => SkyCondition::Clear,
                            23..=53 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        33,
                    )
                }
                PartOfSeason::SecondThird => {
                    temp_base += 57
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -48,
                            BaseTemperatureModifier::SevereRecordLow => -32,
                            BaseTemperatureModifier::RecordLow => -16,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 16,
                            BaseTemperatureModifier::SevereRecordHigh => 32,
                            BaseTemperatureModifier::ExtremeRecordHigh => 48,
                        };

                    (
                        temp_base - rngesus.gen_range(6, 16),
                        temp_base + rngesus.gen_range(6, 16),
                        match rand_sky_condition {
                            0..=34 => SkyCondition::Clear,
                            35..=59 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        36,
                    )
                }
                PartOfSeason::LastThird => {
                    temp_base += 46
                        + match base_temp_modifier {
                            BaseTemperatureModifier::ExtremeRecordLow => -45,
                            BaseTemperatureModifier::SevereRecordLow => -30,
                            BaseTemperatureModifier::RecordLow => -15,
                            BaseTemperatureModifier::Normal => 0,
                            BaseTemperatureModifier::RecordHigh => 17,
                            BaseTemperatureModifier::SevereRecordHigh => 34,
                            BaseTemperatureModifier::ExtremeRecordHigh => 51,
                        };

                    (
                        temp_base - rngesus.gen_range(5, 15),
                        temp_base + rngesus.gen_range(7, 17),
                        match rand_sky_condition {
                            0..=19 => SkyCondition::Clear,
                            20..=49 => SkyCondition::PartlyCloudy,
                            _ => SkyCondition::Cloudy,
                        },
                        40,
                    )
                }
            },
        };

        Weather {
            temp_low,
            temp_high,
            sky_condition,
            precip_chance,
        }
    }
}

#[derive(Debug)]
pub enum PartOfSeason {
    FirstThird,
    SecondThird,
    LastThird,
}

#[derive(Clone, Debug)]
pub enum BaseTemperatureModifier {
    ExtremeRecordLow,
    SevereRecordLow,
    RecordLow,
    Normal,
    RecordHigh,
    SevereRecordHigh,
    ExtremeRecordHigh,
}

impl From<ThreadRng> for BaseTemperatureModifier {
    fn from(mut rng: ThreadRng) -> BaseTemperatureModifier {
        match rng.gen_range(0_usize, 100_usize) {
            0 => BaseTemperatureModifier::ExtremeRecordLow,
            1 => BaseTemperatureModifier::SevereRecordLow,
            2..=3 => BaseTemperatureModifier::RecordLow,
            4..=95 => BaseTemperatureModifier::Normal,
            96..=97 => BaseTemperatureModifier::RecordHigh,
            98 => BaseTemperatureModifier::SevereRecordHigh,
            _ => BaseTemperatureModifier::ExtremeRecordHigh,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum SkyCondition {
    Clear,
    PartlyCloudy,
    Cloudy,
}

#[derive(Clone, Debug, PartialEq)]
pub enum PrecipitationType {
    HeavyBlizzard(PrecipitationEffect),
    Blizzard(PrecipitationEffect),
    HeavySnowstorm(PrecipitationEffect),
    LightSnowstorm(PrecipitationEffect),
    Sleetstorm(PrecipitationEffect),
    Hailstorm(PrecipitationEffect),
    HeavyFog(PrecipitationEffect),
    LightFog(PrecipitationEffect),
    Mist(PrecipitationEffect),
    Drizzle(PrecipitationEffect),
    LightRainstorm(PrecipitationEffect),
    HeavyRainstorm(PrecipitationEffect),
    Thunderstorm(PrecipitationEffect),
    TropicalStorm(PrecipitationEffect),
    Monsoon(PrecipitationEffect),
    Gale(PrecipitationEffect),
    Hurricane(PrecipitationEffect),
    None(usize, usize),
}

impl PrecipitationType {
    pub fn from_temp_and_terrain(
        precip_roll: usize,
        temp_low: isize,
        temp_high: isize,
        terrain: Terrain,
    ) -> Self {
        if precip_roll <= 2 && temp_high <= 10 && terrain != Terrain::Desert {
            PrecipitationType::HeavyBlizzard(PrecipitationEffect::new(
                3,
                24,
                "hours",
                48,
                88,
                VisionObscurity::Heavy,
                5,
                -20,
            ))
        } else if precip_roll <= 5 && temp_high <= 20 && terrain != Terrain::Desert {
            PrecipitationType::Blizzard(PrecipitationEffect::new(
                3,
                24,
                "hours",
                39,
                60,
                VisionObscurity::Heavy,
                10,
                -15,
            ))
        } else if precip_roll <= 10 && temp_high <= 25 {
            PrecipitationType::HeavySnowstorm(PrecipitationEffect::new(
                4,
                24,
                "hours",
                3,
                30,
                VisionObscurity::Heavy,
                20,
                -10,
            ))
        } else if precip_roll <= 20 && temp_high <= 35 {
            PrecipitationType::LightSnowstorm(PrecipitationEffect::new(
                2,
                12,
                "hours",
                4,
                24,
                VisionObscurity::Light,
                25,
                -5,
            ))
        } else if precip_roll <= 25 && temp_high <= 35 {
            PrecipitationType::Sleetstorm(PrecipitationEffect::new(
                1,
                6,
                "hours",
                3,
                30,
                VisionObscurity::Light,
                20,
                -2,
            ))
        } else if precip_roll <= 27 && temp_high <= 65 && terrain != Terrain::Desert {
            PrecipitationType::Hailstorm(PrecipitationEffect::new(
                1,
                4,
                "hours",
                4,
                40,
                VisionObscurity::Light,
                10,
                -5,
            ))
        } else if precip_roll <= 30
            && temp_low >= 20
            && temp_high <= 60
            && terrain != Terrain::Desert
        {
            PrecipitationType::HeavyFog(PrecipitationEffect::new(
                1,
                12,
                "hours",
                1,
                20,
                VisionObscurity::Heavy,
                25,
                -15,
            ))
        } else if precip_roll <= 38
            && temp_low >= 30
            && temp_high <= 70
            && terrain != Terrain::Desert
        {
            PrecipitationType::LightFog(PrecipitationEffect::new(
                2,
                8,
                "hours",
                1,
                10,
                VisionObscurity::Light,
                30,
                -10,
            ))
        } else if precip_roll <= 40 && temp_low >= 30 {
            PrecipitationType::Mist(PrecipitationEffect::new(
                2,
                12,
                "hours",
                1,
                10,
                VisionObscurity::None,
                15,
                0,
            ))
        } else if precip_roll <= 45 && temp_low >= 25 {
            PrecipitationType::Drizzle(PrecipitationEffect::new(
                1,
                10,
                "hours",
                1,
                20,
                VisionObscurity::None,
                20,
                0,
            ))
        } else if precip_roll <= 60 && temp_low >= 25 {
            PrecipitationType::LightRainstorm(PrecipitationEffect::new(
                1,
                12,
                "hours",
                1,
                20,
                VisionObscurity::None,
                45,
                0,
            ))
        } else if precip_roll <= 70 && temp_low >= 25 {
            PrecipitationType::HeavyRainstorm(PrecipitationEffect::new(
                1,
                12,
                "hours",
                12,
                34,
                VisionObscurity::Light,
                30,
                -5,
            ))
        } else if precip_roll <= 84 && temp_low >= 30 {
            PrecipitationType::Thunderstorm(PrecipitationEffect::new(
                1,
                4,
                "hours",
                4,
                40,
                VisionObscurity::Light,
                15,
                -5,
            ))
        } else if precip_roll <= 89
            && temp_low >= 40
            && terrain != Terrain::Desert
            && terrain != Terrain::Plains
        {
            PrecipitationType::TropicalStorm(PrecipitationEffect::new(
                1,
                3,
                "days",
                33,
                66,
                VisionObscurity::Heavy,
                20,
                -5,
            ))
        } else if precip_roll <= 94
            && temp_low >= 55
            && terrain != Terrain::Desert
            && terrain != Terrain::Plains
        {
            PrecipitationType::Monsoon(PrecipitationEffect::new(
                7,
                13,
                "days",
                6,
                60,
                VisionObscurity::Heavy,
                30,
                -10,
            ))
        } else if precip_roll <= 97 && temp_low >= 40 && terrain != Terrain::Desert {
            PrecipitationType::Gale(PrecipitationEffect::new(
                1,
                3,
                "days",
                46,
                88,
                VisionObscurity::Heavy,
                15,
                -15,
            ))
        } else if precip_roll <= 99 && temp_low >= 55 && terrain != Terrain::Desert {
            PrecipitationType::Hurricane(PrecipitationEffect::new(
                1,
                4,
                "days",
                77,
                140,
                VisionObscurity::Heavy,
                20,
                -20,
            ))
        } else {
            PrecipitationType::None(1, 19)
        }
    }

    pub fn get_effect(&self) -> Option<&PrecipitationEffect> {
        match self {
            PrecipitationType::HeavyBlizzard(effect)
            | PrecipitationType::Blizzard(effect)
            | PrecipitationType::HeavySnowstorm(effect)
            | PrecipitationType::LightSnowstorm(effect)
            | PrecipitationType::Sleetstorm(effect)
            | PrecipitationType::Hailstorm(effect)
            | PrecipitationType::HeavyFog(effect)
            | PrecipitationType::LightFog(effect)
            | PrecipitationType::Mist(effect)
            | PrecipitationType::Drizzle(effect)
            | PrecipitationType::LightRainstorm(effect)
            | PrecipitationType::HeavyRainstorm(effect)
            | PrecipitationType::Thunderstorm(effect)
            | PrecipitationType::TropicalStorm(effect)
            | PrecipitationType::Monsoon(effect)
            | PrecipitationType::Gale(effect)
            | PrecipitationType::Hurricane(effect) => Some(effect),
            _ => None,
        }
    }

    pub fn get_prev_precip_roll(&self) -> PrecipitationType {
        match self {
            PrecipitationType::HeavyBlizzard(effect) => {
                PrecipitationType::HeavyBlizzard(effect.clone())
            }
            PrecipitationType::Blizzard(effect) => PrecipitationType::HeavyBlizzard(effect.clone()),
            PrecipitationType::HeavySnowstorm(effect) => {
                PrecipitationType::Blizzard(effect.clone())
            }
            PrecipitationType::LightSnowstorm(effect) => {
                PrecipitationType::HeavySnowstorm(effect.clone())
            }
            PrecipitationType::Sleetstorm(effect) => {
                PrecipitationType::LightSnowstorm(effect.clone())
            }
            PrecipitationType::Hailstorm(effect) => PrecipitationType::Sleetstorm(effect.clone()),
            PrecipitationType::HeavyFog(effect) => PrecipitationType::Hailstorm(effect.clone()),
            PrecipitationType::LightFog(effect) => PrecipitationType::HeavyFog(effect.clone()),
            PrecipitationType::Mist(effect) => PrecipitationType::LightFog(effect.clone()),
            PrecipitationType::Drizzle(effect) => PrecipitationType::Mist(effect.clone()),
            PrecipitationType::LightRainstorm(effect) => PrecipitationType::Drizzle(effect.clone()),
            PrecipitationType::HeavyRainstorm(effect) => {
                PrecipitationType::LightRainstorm(effect.clone())
            }
            PrecipitationType::Thunderstorm(effect) => {
                PrecipitationType::HeavyRainstorm(effect.clone())
            }
            PrecipitationType::TropicalStorm(effect) => {
                PrecipitationType::Thunderstorm(effect.clone())
            }
            PrecipitationType::Monsoon(effect) => PrecipitationType::TropicalStorm(effect.clone()),
            PrecipitationType::Gale(effect) => PrecipitationType::Monsoon(effect.clone()),
            PrecipitationType::Hurricane(effect) => PrecipitationType::Gale(effect.clone()),
            PrecipitationType::None(wind_speed_min, wind_speed_max) => {
                PrecipitationType::None(*wind_speed_min, *wind_speed_max)
            }
        }
    }

    pub fn get_next_precip_roll(&self) -> PrecipitationType {
        match self {
            PrecipitationType::HeavyBlizzard(effect) => PrecipitationType::Blizzard(effect.clone()),
            PrecipitationType::Blizzard(effect) => {
                PrecipitationType::HeavySnowstorm(effect.clone())
            }
            PrecipitationType::HeavySnowstorm(effect) => {
                PrecipitationType::LightSnowstorm(effect.clone())
            }
            PrecipitationType::LightSnowstorm(effect) => {
                PrecipitationType::Sleetstorm(effect.clone())
            }
            PrecipitationType::Sleetstorm(effect) => PrecipitationType::Hailstorm(effect.clone()),
            PrecipitationType::Hailstorm(effect) => PrecipitationType::HeavyFog(effect.clone()),
            PrecipitationType::HeavyFog(effect) => PrecipitationType::LightFog(effect.clone()),
            PrecipitationType::LightFog(effect) => PrecipitationType::Mist(effect.clone()),
            PrecipitationType::Mist(effect) => PrecipitationType::Drizzle(effect.clone()),
            PrecipitationType::Drizzle(effect) => PrecipitationType::LightRainstorm(effect.clone()),
            PrecipitationType::LightRainstorm(effect) => {
                PrecipitationType::HeavyRainstorm(effect.clone())
            }
            PrecipitationType::HeavyRainstorm(effect) => {
                PrecipitationType::Thunderstorm(effect.clone())
            }
            PrecipitationType::Thunderstorm(effect) => {
                PrecipitationType::TropicalStorm(effect.clone())
            }
            PrecipitationType::TropicalStorm(effect) => PrecipitationType::Monsoon(effect.clone()),
            PrecipitationType::Monsoon(effect) => PrecipitationType::Gale(effect.clone()),
            PrecipitationType::Gale(effect) => PrecipitationType::Hurricane(effect.clone()),
            PrecipitationType::Hurricane(effect) => PrecipitationType::Hurricane(effect.clone()),
            PrecipitationType::None(wind_speed_min, wind_speed_max) => {
                PrecipitationType::None(*wind_speed_min, *wind_speed_max)
            }
        }
    }
}

impl ToString for PrecipitationType {
    fn to_string(&self) -> String {
        match self {
            PrecipitationType::HeavyBlizzard(_) => "a heavy blizzard",
            PrecipitationType::Blizzard(_) => "a blizzard",
            PrecipitationType::HeavySnowstorm(_) => "a heavy snowstorm",
            PrecipitationType::LightSnowstorm(_) => "a light snowstorm",
            PrecipitationType::Sleetstorm(_) => "a sleet storm",
            PrecipitationType::Hailstorm(_) => "a hailstorm",
            PrecipitationType::HeavyFog(_) => "heavy fog",
            PrecipitationType::LightFog(_) => "light fog",
            PrecipitationType::Mist(_) => "mist",
            PrecipitationType::Drizzle(_) => "drizzle",
            PrecipitationType::LightRainstorm(_) => "a light rainstorm",
            PrecipitationType::HeavyRainstorm(_) => "a heavy rainstorm",
            PrecipitationType::Thunderstorm(_) => "a thunderstorm",
            PrecipitationType::TropicalStorm(_) => "a tropical storm",
            PrecipitationType::Monsoon(_) => "a monsoon",
            PrecipitationType::Gale(_) => "heavy gales",
            PrecipitationType::Hurricane(_) => "hurricane-force rain and wind",
            PrecipitationType::None(_, _) => "no precipitation",
        }
        .to_owned()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct PrecipitationEffect {
    pub duration_min: usize,
    pub duration_max: usize,
    pub duration_type: &'static str,
    pub wind_speed_min: usize,
    pub wind_speed_max: usize,
    pub vision_obscurity: VisionObscurity,
    pub continuing_chance: usize,
    pub navigation_penalty: isize,
}

impl PrecipitationEffect {
    pub fn new(
        duration_min: usize,
        duration_max: usize,
        duration_type: &'static str,
        wind_speed_min: usize,
        wind_speed_max: usize,
        vision_obscurity: VisionObscurity,
        continuing_chance: usize,
        navigation_penalty: isize,
    ) -> Self {
        PrecipitationEffect {
            duration_min,
            duration_max,
            duration_type,
            wind_speed_min,
            wind_speed_max,
            vision_obscurity,
            continuing_chance,
            navigation_penalty,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum VisionObscurity {
    None,
    Light,
    Heavy,
}

pub fn gen_weather(months: &[Month], colder: usize, hotter: usize) -> Vec<Weather> {
    let div = months.iter().map(|month| month.days).sum::<usize>() / 12;
    let mut cont = 0_usize;
    let mut temperature_modifier = BaseTemperatureModifier::from(thread_rng());
    let mut global_season = &months.first().unwrap().season;
    let mut days: usize = 0;

    months
        .iter()
        .map(|month| iter::repeat(&month.season).take(month.days))
        .flatten()
        .map(|season| {
            let part_of_season = if season != global_season {
                global_season = season;
                days = 0;
                PartOfSeason::FirstThird
            } else {
                days = days + 1;
                match days / div {
                    0 => PartOfSeason::FirstThird,
                    1 => PartOfSeason::SecondThird,
                    _ => PartOfSeason::LastThird,
                }
            };

            Weather::new(
                season.clone(),
                part_of_season,
                if cont > 0 {
                    cont = cont - 1;
                    temperature_modifier.clone()
                } else {
                    temperature_modifier = BaseTemperatureModifier::from(thread_rng());
                    match temperature_modifier {
                        BaseTemperatureModifier::Normal => BaseTemperatureModifier::Normal,
                        _ => {
                            cont = match thread_rng().gen_range(0_usize, 20_usize) {
                                0 => 1,
                                1 | 2 => 2,
                                3..=9 => 3,
                                10..=13 => 4,
                                14..=16 => 5,
                                17 | 18 => 6,
                                _ => 7,
                            };
                            temperature_modifier.clone()
                        }
                    }
                },
                colder,
                hotter,
            )
        })
        .collect::<Vec<Weather>>()
}
