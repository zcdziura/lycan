use std::error::Error;
use std::fmt;
use std::str::FromStr;

use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize)]
pub struct Calendar {
    pub months: Vec<Month>,
    pub week_days: Vec<String>,
    pub first_day: usize,
}

impl Calendar {
    pub fn new(months: Vec<Month>, week_days: Vec<String>, first_day: usize) -> Self {
        Calendar {
            months,
            week_days,
            first_day,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Month {
    pub name: String,
    pub days: usize,
    pub season: Season,
}

impl Month {
    pub fn new(name: &str, days: usize, season: Season) -> Self {
        Month {
            name: name.to_owned(),
            days,
            season,
        }
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum Season {
    Winter,
    Spring,
    Summer,
    Autumn,
}

impl FromStr for Season {
    type Err = ParseSeasonError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Winter" => Ok(Self::Winter),
            "Spring" => Ok(Self::Spring),
            "Summer" => Ok(Self::Summer),
            "Autumn" => Ok(Self::Autumn),
            _ => Err(ParseSeasonError {
                cause: s.to_owned(),
            }),
        }
    }
}

#[derive(Debug)]
pub struct ParseSeasonError {
    cause: String,
}

impl fmt::Display for ParseSeasonError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error: \"{}\" is not a valid Season", self.cause)
    }
}

impl Error for ParseSeasonError {}
