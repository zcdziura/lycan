use std::convert::From;
use std::fmt;

use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize)]
pub struct CelestialEvents {
    pub moons: Vec<Moon>,
}

impl CelestialEvents {
    pub fn new(moons: Vec<Moon>) -> Self {
        CelestialEvents { moons }
    }
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Moon {
    pub name: String,
    pub cycle: usize,
    pub shift: usize,
}

impl Moon {
    pub fn new(name: &str, cycle: usize, shift: usize) -> Self {
        Moon {
            name: name.to_owned(),
            cycle,
            shift,
        }
    }

    pub fn get_phase(&self, day_of_year: usize) -> MoonPhase {
        let phase_point_idx = self.cycle / 8;
        let point_in_cycle = ((day_of_year + self.shift) / phase_point_idx) % 8;
        MoonPhase::from(point_in_cycle)
    }
}

#[derive(Clone, Debug)]
pub enum MoonPhase {
    New,
    WaxingCrescent,
    FirstQuarter,
    WaxingGibbous,
    Full,
    WaningGibbous,
    ThirdQuarter,
    WaningCrescent,
}

impl From<usize> for MoonPhase {
    fn from(cycle_point: usize) -> Self {
        match cycle_point {
            0 => Self::New,
            1 => Self::WaxingCrescent,
            2 => Self::FirstQuarter,
            3 => Self::WaxingGibbous,
            4 => Self::Full,
            5 => Self::WaningGibbous,
            6 => Self::ThirdQuarter,
            7 => Self::WaningCrescent,
            _ => unreachable!(),
        }
    }
}

impl From<MoonPhase> for usize {
    fn from(phase: MoonPhase) -> Self {
        match phase {
            MoonPhase::New => 0,
            MoonPhase::WaxingCrescent => 1,
            MoonPhase::FirstQuarter => 2,
            MoonPhase::WaxingGibbous => 3,
            MoonPhase::Full => 4,
            MoonPhase::WaningGibbous => 5,
            MoonPhase::ThirdQuarter => 6,
            MoonPhase::WaningCrescent => 7,
        }
    }
}

impl fmt::Display for MoonPhase {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::New => "in shadow and hidden from sight",
                Self::WaxingCrescent => "in waxing crescent, beginning to appear in the night",
                Self::FirstQuarter => "in the half-light of the first quarter",
                Self::WaxingGibbous => "in waxing gibbous, nearly fully lit",
                Self::Full => "fully lit in all its splendor",
                Self::WaningGibbous => "in waning gibbous, beginning to retreat into shadow",
                Self::ThirdQuarter => "in the half-shadow of the third quarter",
                Self::WaningCrescent => "in waning crescent, nearly hidden in shadow",
            }
        )
    }
}
