use std::io::{self, Write};
use std::process;

use colored::*;
use numeral::Cardinal;

pub fn get_input(prompt: &str) -> String {
    print!("{} ", prompt);
    io::stdout()
        .flush()
        .expect("Unable to flush stdout when printing prompt");
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("Unable to read line from stdin");

    buffer.trim().to_owned()
}

pub fn get_cardinal(number: u8) -> String {
    number
        .cardinal()
        .split(|c| c == ' ' || c == '-')
        .map(|word| match word {
            "one" => "first".to_owned(),
            "two" => "second".to_owned(),
            "three" => "third".to_owned(),
            "five" => "fifth".to_owned(),
            "eight" => "eighth".to_owned(),
            "nine" => "ninth".to_owned(),
            "twelve" => "twelfth".to_owned(),
            "twenty" => "twentieth".to_owned(),
            _ => format!("{}th", word),
        })
        .collect::<Vec<String>>()
        .join(" ")
}

pub fn error(message: &str) -> ! {
    eprintln!("{}", format!("Error: {}", message).red().bold());
    process::exit(1);
}
